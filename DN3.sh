#!/bin/bash
count=0
function output_for_pid {
  local comm
  local pid
  local ni
  local ppid
  OIFS=$IFS
  IFS=$'|'
  read -r comm pid ni ppid  <<< $(ps --noheaders --pid $1 -o command -o "|%p|%n|%P")
  IFS=$OIFS
  pid=$(echo $pid | awk '{$1=$1}1')
  ni=$(echo $ni | awk '{$1=$1}1')
  comm=$(echo "$comm" | awk '{$1=$1}1')
  local my_count=$(echo $count)
  ((count=count+1))
  if [[ "$ppid" -ne 0 ]];
  then
    output_for_pid $ppid
  fi
  tmp=$(echo $comm | awk '{print $1;}')
  name=$(echo $(basename $tmp))
  get_color $my_count
  barva=$Result
  printf "\e[38;5;%sm%s\t%s\t%s\t%s\n" "$barva" "$name" "$pid" "$comm" "$ni"
  printf "\e[38;5;255m"
}

function get_color {
  ((tmp=count-1))
  if [[ $1 -eq $tmp ]]
  then
    barva="16"
  elif [[ $1 -eq 0 ]]
  then
    barva="21"
  else
    round 5 $tmp
    ((barva=21-Result*my_count))
  fi
  Result=$barva
}

function round {
  Result=$((($1 + $2/2) / $2))
}

function output_prompt {
  printf "\n"
  echo "[u] poskusi ubiti, [+] povišaj prijaznost, [-] znižaj prijaznost, [s] pošlji signal" 
}

function change_niceness {
  cur_nice=$(ps --pid $pid -o ni=)
  
  if [[ $1 -eq 0 ]]
  then
    ((cur_nice=cur_nice+1))
  else
    ((cur_nice=cur_nice-1))
  fi

  renice $cur_nice -p $pid > /dev/null
}

function send_message {
  echo "" 
  read -p "Vnesite vsebino signala: " line
  kill -$line $pid
}

function is_alive {
    v_is_alive=$(ps --no-header -p $1 -o command)
    if [ -z "$v_is_alive" ]
    then
      Result=0
    else
      if [[ $v_is_alive =~ "<defunct>" ]]
      then
        Result=0
      else
        Result=1
      fi
    fi
}

function get_pid {
  valid=0
  local t_pid
  read -p "Vnesite PID: " t_pid
  is_alive $t_pid
  valid=$Result
  while [ $valid -eq 0 ]
  do
    clear
    echo -e "$(tput setaf 1)Neveljaven PID$(tput sgr 0)"
    read -p "Vnesite PID: " t_pid
    is_alive $t_pid
    valid=$Result
    clear

  done
  Result=$t_pid
}

function check_if_alive_and_act {
  is_alive $pid
  if [ $Result -eq 0 ]
  then
    clear
    echo "Proces je mrtev, ali pojdeš na pogreb?"
    get_pid
    pid=$Result
  fi
}

clear
get_pid
pid=$Result
count=0
output_for_pid $pid
output_prompt


while read -n1 -s comm ; do
  check_if_alive_and_act
  case $comm in
  u) kill -SIGTERM $pid;;
  +) change_niceness 0;;
  -) change_niceness 1;;
  s) send_message;;
  esac
  check_if_alive_and_act
  clear
  count=0
  output_for_pid $pid
  output_prompt
  
done
