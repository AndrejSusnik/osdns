#!/bin/bash

#ustvarimo asociativno tabele, nato iteritamo skozi datoteke, vrstico po vrstico
#na vsakem koraku vrednost asociativne tabele na indexu ki ga predstavlja prebrana vrstica
#povecamo za 1
index=0
declare -A arr
while read line; do
   ((arr["$line"]=arr["$line"]+1))
   ((index=index+1))
done < "$1"

index=0
for i in "${!arr[@]}"
do
   m_arr[index]="$i"
   ((index=index+1))
done

IFS=$'\n' sorted=($(sort <<<"${m_arr[*]}"))
unset IFS


for I in "${sorted[@]}"
do
if [ $# -eq 1 ]; then
   echo "${arr[$I]}" "$I" 
fi
if [ $# -eq 2 ]; then

   echo "${arr[$I]}" "$I" >> "$2"
fi
done