#!/bin/bash
if [ $# -lt 2 ]
then
    exit 21
fi

find $1 -type d | while read -r line ; do
mkdir -p $2/$(realpath --relative-to $1 $line ) || exit 21
done
ln -s $2 $HOME/škorenj
chmod 1100 $2
chmod -R 0550 $2 
