#!/bin/bash

if [ "$#" -lt 2 ];then
    echo "Not enough params"
    exit
fi
arr=( "$@" )

index=1
for var in "$@" 
do
    if [ "$var" == "-u" ]
    then
        username=${arr[$index]}
    fi
    if [ "$var" == "--superuser" ]
    then
        super=1
    fi
    
    ((index=index+1))
done 

if [ -z "$username" ]
then
    echo "username not set"
    break
fi

read -p "Vnesite geslo za uporabnika" $username -s password
if [ -z "$super" ]
then
sudo useradd -m -d /home/"$username" -p $(echo $password | openssl passwd -6 -stdin) -s /bin/bash "$username"
else
sudo useradd -m -d /home/"$username" -p $(echo $password | openssl passwd -6 -stdin) -G sudo -s /bin/bash "$username"
fi
echo "Uporabnik" $username "ustvarjen."