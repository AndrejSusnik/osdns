#!/bin/bash

# function isNum
# če je možna primerjava argumenta z stikalom -eq potem je argument število
# če pa je argument niz potem bo preverjanje vrglo napake ki jo zadušimo z 2>/dev/null
# Returns: true če je arguemnt številka, false če je argument niz
isNum (){
    if [ "$1" -eq "$1" ] 2>/dev/null;
    then
        Result="true"
    else    
        Result="false"
    fi
}

# function smallerTime
# Returns: manjšega od argumentov
smallerTime (){
    if [ "$1" -lt "$2" ];
    then
        Result=$2
    else
        Result=$1
    fi
}

# function getLinkName
# Returns: vrne ime simbolične povezave
getLinkName (){
    sizeFormated=$(printf "%0${format}d" ${sizeInMb})
    getLinkPath $1 $sizeInMb
    Result="$Result/${sizeFormated}-$1"
}

# function getTimeDifferenceInDays
# Returns: Razliko med trenutnim datumom in med manjšim datumom v argumentu 1 in 2 v dneh
getTimeDifferenceInDays(){
    currentTime=$(date +%s)
    dateA=$(date -d $1 +%s) #Čas v sekundah
    dateM=$(date -d $2 +%s) #Čas v sekundah
    smallerTime "${dateA%.*}" "${dateM%.*}" #%.* Odstrani vsa decimalna mesta
    timeAM=$Result
    seconds=$[currentTime - Result]
    daysSinceLastModification=$[seconds / (3600 * 24)]
    Result=$(echo "${daysSinceLastModification%%.*}")
}

getLinkPath (){
     if [ -z $outputType ];
    then
       Result="out"
    else
        if [ $outputType = "type" ];
        then
            if [[ $1 == *"."* ]]
            then
                ext="${1##*.}"
                linkPath="out/$ext"
                mkdir -p $linkPath
                tmpSize=$2
                sizeByType["$ext"]=$[ sizeByType["$ext"] + tmpSize]
                Result=$linkPath
            else
                linkPath="out"
                Result=$linkPath
            fi
        else
            mul=$[($2 - $size) / $outputType]
            class=$[ $mul * outputType + 100 ]
            linkPath="out/$class"
            mkdir -p $linkPath
            tmpSize=$2
            sizeByType["$class"]=$[ sizeByType["$class"] + tmpSize]
            Result=$linkPath
        fi
    fi
}

if [ -d $1 ]; then
    searchDir=$1
    shift
else
    searchDir=$PWD
fi

while getopts ":s:t:d:o:" option; do
    case $option in
        s) size=$OPTARG
           isNum "$size"
           if [ $Result = "false" ]
           then
            echo "argument ni numericna vrednost" 1>&2
            exit 1
           fi
            ;;
        t) notUsed=$OPTARG;;
        d) format=$OPTARG;;
        o) outputType=$OPTARG;;
        *) echo "Stikalo ne obstaja" 1>&2
           exit 1;;
    esac
done

if [ -z $size ]; then
    echo "niste podali obveznega stikala -s" 1>&2
    exit 22
fi
declare -A files
declare -A sizeByType
skupaj=0
mkdir out
if [ -z $format ];
then
    format=4
fi
while read -r line 
do
    IFS=";"
    read -r -a array <<< $line
    
    getTimeDifferenceInDays ${array[3]} ${array[4]}
    timeInDays=$Result

    if [ -z $notUsed ] || [ $timeInDays -lt $notUsed ]
    then
    absPath=$(realpath ${array[1]})
    
    filename=${array[0]}

    sizeInMb=$[array[2] / 1024]
    skupaj=$[$skupaj + $sizeInMb]
    
    OUT=$(printf "%s %s (%s MB) (%s)\n" ${filename} ${absPath} ${sizeInMb} ${timeInDays})
    echo "$OUT"
    files["$OUT"]=$sizeInMb
    getLinkName ${filename}
    linkName=$Result
    ln -s ${absPath} ${linkName}
    fi
done < <(find "$searchDir" -size +${size}M -printf "%f;%h;%k;%a;%t\n") 2>/dev/null
echo "---"

for ind in "${!files[@]}"; do
    echo $ind
done | awk -F "[()]" '{print $2, $0}' | sort -t ' ' -r -k1,1nr | cut -d' ' -f3-

echo "---"

if [ ! -z $outputType ];
then
    for key in "${!sizeByType[@]}"; do
        echo "$key ${sizeByType[$key]} MB"
    done | sort -t ' ' -r -k2,2nr
    echo "---"
fi
echo "skupaj ${skupaj} MB"